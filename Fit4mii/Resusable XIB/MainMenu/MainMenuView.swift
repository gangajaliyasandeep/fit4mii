//
//  ManinMenuView.swift
//  Fit4mii
//
//  Created by Sandeep Gangajaliya on 22/03/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit

class MainMenuView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib ()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib ()
    }
    
    // MARK:- LoadViewFromNib
    func loadViewFromNib()
    {
        let bundle = Bundle(for: type(of: self))
//        let nib = UINib(nibName: "MainMenuView", bundle: bundle)
        guard let view = UINib(nibName: "MainMenuView", bundle: bundle).instantiate(withOwner: self, options: nil)[0] as? UIView else{return}
//        guard let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView else{return}
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
    }

}
