//
//  EventVC.swift
//  Fit4mii
//
//  Created by Sandeep Gangajaliya on 28/02/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit
import MediaPlayer
import GoogleMobileAds

class EventVC: UIViewController, UITableViewDataSource, UITableViewDelegate,GADInterstitialDelegate{

    //MARK:- Outlet Declaration
    
//    781315052033054
//    197216780461144
    let strPageId = "197216780461144"
    
    @IBOutlet var tblList: UITableView!
    
    @IBOutlet var btnMenu: UIButton!
    
    @IBOutlet var lblNoLiveSession: UILabel!
    
    //MARK: Footer View
    @IBOutlet var btnVideos: UIButton!
    @IBOutlet var btnLive: UIButton!
    @IBOutlet var btnEvents: UIButton!
    
    @IBOutlet var lblLiveTitle: UILabel!
    
    @IBOutlet var imgVideos: UIImageView!
    @IBOutlet var imgLive: UIImageView!
    @IBOutlet var imgEvents: UIImageView!
    
    //MARK: Main Menu
    @IBOutlet var viewMainMenu: UIView!
    @IBOutlet var scrlMainMenu: UIScrollView!
    
    //MARK: Main Menu Inn
    @IBOutlet var viewMainMenuInn: UIView!
    @IBOutlet var imgMainMenuInn: UIImageView!
    @IBOutlet var imgMainMenuInnBG: UIImageView!
    
    @IBOutlet var lblLogout: UILabel!
    
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var imgUserPhoto: UIImageView!
    
    @IBOutlet var constMainMenuInnWidth: NSLayoutConstraint!
    
    //MARK: Weight Track
    @IBOutlet var imgWeightTrack: UIImageView!
    @IBOutlet var imgWeightTrackBG: UIImageView!
    
    @IBOutlet var txtWaistWeightTrack: UITextField!
    @IBOutlet var txtHipsWeightTrack: UITextField!
    @IBOutlet var txtArmWeightTrack: UITextField!
    @IBOutlet var txtThighWeightTrack: UITextField!
    @IBOutlet var txtWeightWeightTrack: UITextField!
    
    @IBOutlet var btnSaveWeightTrack: UIButton!
    
    //MARK: BMI
    @IBOutlet var imgBMI: UIImageView!
    @IBOutlet var imgBMIBG: UIImageView!
    
    @IBOutlet var txtAge: UITextField!
    @IBOutlet var txtGender: UITextField!
    @IBOutlet var txtWeight: UITextField!
    @IBOutlet var txtHeight: UITextField!
    
    @IBOutlet var btnGender: UIButton!
    @IBOutlet var btnCaluculateBMI: UIButton!
    
    @IBOutlet var lblBMIStatus: UILabel!
    
    @IBOutlet var constGenderViewHeight: NSLayoutConstraint!
    
    //MARK: Fit4Mii
    @IBOutlet var imgFit4Mii: UIImageView!
    @IBOutlet var imgFit4MiiBG: UIImageView!
    
    //MARK: Weekly Status
    @IBOutlet var tblWeeklyStatus: UITableView!
    @IBOutlet var imgWeeklyStatus: UIImageView!
    @IBOutlet var imgWeeklyStatusBG: UIImageView!
    
    //MARK: Feedback
    @IBOutlet var imgFeedback: UIImageView!
    @IBOutlet var imgFeedbackBGPopUp: UIImageView!
    
    @IBOutlet var txtEmailFeedback: UITextField!
    @IBOutlet var txtViewFeedback: UITextView!
    
    @IBOutlet var imgEmailFeedbackBG: UIImageView!
    @IBOutlet var imgFeedbackBG: UIImageView!
    
    //MARK: Variable Declaration
    var dictPageData: NSDictionary = NSDictionary()
    var dictEventData: NSDictionary = NSDictionary()
    var arrVideoList = NSMutableArray()
    var arrLiveVideoData = NSMutableArray()
    
    var arrLikeCommentCountList = NSMutableArray()
    var arrLikeCommentLiveVideoCountList = NSMutableArray()
    var interstitial: GADInterstitial!
    
    var arrWeeklyStatus = NSMutableArray()
    
    var videoTimer: Timer!
    var isBlinking = false
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblList.rowHeight = UITableViewAutomaticDimension
        tblList.estimatedRowHeight = 237.0
        
        tblList.delegate = self
        tblList.dataSource = self
        
        self.initialization()
        
        NotificationCenter.default.addObserver(self, selector: #selector(EventVC.commentNotification(notification:)), name: NSNotification.Name(rawValue: "commentNotification"), object: nil)
        
        super.viewDidLoad()
        interstitial = createAndLoadInterstitial()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Constants.appDelegate.isFullScreen = false
        
        videoTimer = Timer.scheduledTimer(timeInterval: 100, target: self, selector: #selector(self.fetchVideos), userInfo: nil, repeats: true)
        
        if UIApplication.shared.statusBarOrientation != UIInterfaceOrientation.portrait {
            let value = UIInterfaceOrientation.portrait.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
        }
        
        UIApplication.shared.isStatusBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        videoTimer.invalidate()
    }
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-4067556366564738/9244465206")
        interstitial.delegate = self
        let request = GADRequest()
        interstitial.load(request)
        request.testDevices = [ kGADSimulatorID, "2077ef9a63d2b398840261c8221a0c9a" ]
        return interstitial
    }
    
    
    //MARK:- Initialization
    
    func initialization(){
        
        viewMainMenu.frame = CGRect.init(x: 0, y: 0, width: Constants.ScreenSize.SCREEN_WIDTH, height: 0)
        self.view.addSubview(viewMainMenu)
        
        constMainMenuInnWidth.constant = Constants.ScreenSize.SCREEN_WIDTH
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(self.tapGesture(_:)))
        imgMainMenuInn.addGestureRecognizer(tapGesture1)
        
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(self.tapGesture(_:)))
        imgWeightTrack.addGestureRecognizer(tapGesture2)
        
        let tapGesture3 = UITapGestureRecognizer(target: self, action: #selector(self.tapGesture(_:)))
        imgBMI.addGestureRecognizer(tapGesture3)
        
        let tapGesture4 = UITapGestureRecognizer(target: self, action: #selector(self.tapGesture(_:)))
        imgFit4Mii.addGestureRecognizer(tapGesture4)
        
        let tapGesture5 = UITapGestureRecognizer(target: self, action: #selector(self.tapGesture(_:)))
        imgWeeklyStatus.addGestureRecognizer(tapGesture5)
        
        let tapGesture6 = UITapGestureRecognizer(target: self, action: #selector(self.tapGesture(_:)))
        imgFeedback.addGestureRecognizer(tapGesture6)
        
        imgEmailFeedbackBG.layer.borderWidth = 1.0
        imgEmailFeedbackBG.layer.borderColor = UIColor.white.cgColor
        
        imgFeedbackBG.layer.borderWidth = 1.0
        imgFeedbackBG.layer.borderColor = UIColor.white.cgColor
        
        ProjectUtility.imageShadow(image: imgMainMenuInnBG)
        ProjectUtility.imageShadow(image: imgWeightTrack)
        ProjectUtility.imageShadow(image: imgBMIBG)
        ProjectUtility.imageShadow(image: imgWeeklyStatusBG)
        ProjectUtility.imageShadow(image: imgFit4MiiBG)
        ProjectUtility.imageShadow(image: imgFeedbackBGPopUp)
        
        self.displayData()

    }

    func tapGesture(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.viewMainMenu.frame = CGRect.init(x: 0, y: 0, width: Constants.ScreenSize.SCREEN_WIDTH, height: 0)
            self.view.layoutIfNeeded()
        })
        btnMenu.isSelected = false
    }
    
    func displayData(){
        
        if(FBSDKAccessToken.current() != nil){
            
            lblUserName.text = Constants.appDelegate.dictFBUserDetail.value(forKey: "name") as? String
            imgUserPhoto.setImageWith(NSURL(string: Constants.appDelegate.dictFBUserDetail.value(forKey: "profilePic") as! String) as URL!)
            
            imgUserPhoto.layer.cornerRadius = 38.0
            imgUserPhoto.clipsToBounds = true
            
            self.fetchPageDetail()
            
            lblLogout.text = "Logout"
        }else{
            
            lblLogout.text = "Login"
        }
    }
    
    // MARK:- Webservice Call
    
    func addWeightTrackJson()
    {
        ProjectUtility.loadingShow()
        
        var strEdit = "0"
        if self.btnSaveWeightTrack.currentTitle == "Update" {
            strEdit = "1"
        }
        
        let dict = ["user_id":Constants.appDelegate.dictLoginUserDetail.value(forKey: "user_id")!, "waist":txtWaistWeightTrack.text!, "hips":txtHipsWeightTrack.text!, "arm":txtArmWeightTrack.text!, "thigh":txtThighWeightTrack.text!, "weight":txtWeightWeightTrack.text!, "os_type":"1", "version":"1.0", "is_edit":strEdit]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/track/weighttrack"
        print("\(strUrl) == \(dict)")
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            ProjectUtility.loadingHide()
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    print("response = ",dictData)
                    
                    ProjectUtility.displayTost(erroemessage: dictData.value(forKey: "message") as! String)
                    
                    if dictData.value(forKey: "success") as! Bool == true{
                        
                    }
                    
                    return
                }
            }
            
            ProjectUtility.displayTost(erroemessage: "We are are working on improvements. Please stay attached.")
        }) { (error) -> Void in
            
            print("error = ",error)
            ProjectUtility.loadingHide()
            ProjectUtility.displayTost(erroemessage: "We are are working on improvements. Please stay attached.")
        }
    }
    
    func checkWeightTrackJson()
    {
        ProjectUtility.loadingShow()
        
        let dict = ["user_id":Constants.appDelegate.dictLoginUserDetail.value(forKey: "user_id")!, "os_type":"1", "version":"1.0", "is_edit":"0"]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/track/currentweek"
        print("\(strUrl) == \(dict)")
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            ProjectUtility.loadingHide()
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    print("response = ",dictData)
                    
                    if dictData.value(forKey: "success") as! Bool == true{
                        
                        self.txtWaistWeightTrack.text = ""
                        self.txtHipsWeightTrack.text = ""
                        self.txtArmWeightTrack.text = ""
                        self.txtThighWeightTrack.text = ""
                        self.txtWeightWeightTrack.text = ""
                        
                        self.txtWaistWeightTrack.text = NSString(format: "%.2f", (dictData.value(forKey: "data") as! NSDictionary).value(forKey: "waist") as! CGFloat) as String
                        
                        self.txtHipsWeightTrack.text = NSString(format: "%.2f", (dictData.value(forKey: "data") as! NSDictionary).value(forKey: "hips") as! CGFloat) as String
                        
                        self.txtArmWeightTrack.text = NSString(format: "%.2f", (dictData.value(forKey: "data") as! NSDictionary).value(forKey: "arm") as! CGFloat) as String
                        
                        self.txtThighWeightTrack.text = NSString(format: "%.2f", (dictData.value(forKey: "data") as! NSDictionary).value(forKey: "thigh") as! CGFloat) as String
                        
                        self.txtWeightWeightTrack.text = NSString(format: "%.2f", (dictData.value(forKey: "data") as! NSDictionary).value(forKey: "weight") as! CGFloat) as String
                        
                        self.btnSaveWeightTrack.setTitle("Update", for: UIControlState.normal)
                    }
                    
                    return
                }
            }
            
            ProjectUtility.displayTost(erroemessage: "We are are working on improvements. Please stay attached.")
        }) { (error) -> Void in
            
            print("error = ",error)
            ProjectUtility.loadingHide()
            ProjectUtility.displayTost(erroemessage: "We are are working on improvements. Please stay attached.")
        }
    }
    
    func listOfWightTrackJson()
    {
        ProjectUtility.loadingShow()
        
        let dict = ["user_id":Constants.appDelegate.dictLoginUserDetail.value(forKey: "user_id")!, "page":"0", "os_type":"1", "version":"1.0", "is_edit":"0"]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/track/listoftrack"
        print("\(strUrl) == \(dict)")
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            ProjectUtility.loadingHide()
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    print("response = ",dictData)
                    
                    if dictData.value(forKey: "success") as! Bool == true{
                        
                        self.arrWeeklyStatus = (dictData.value(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                        
                        self.tblWeeklyStatus.reloadData()
                    }
                    
                    return
                }
            }
            
            ProjectUtility.displayTost(erroemessage: "Weekly track is not available.")
        }) { (error) -> Void in
            
            print("error = ",error)
            ProjectUtility.loadingHide()
            ProjectUtility.displayTost(erroemessage: "We are are working on improvements. Please stay attached.")
        }
    }
    
    func sendFeedBackJson()
    {
        ProjectUtility.loadingShow()
        
        let dict = ["email_address":txtEmailFeedback.text!, "feedback":txtViewFeedback.text!]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/message/sendfeedback"
        print("\(strUrl) == \(dict)")
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            ProjectUtility.loadingHide()
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    print("response = ",dictData)
                    
                    ProjectUtility.displayTost(erroemessage: dictData.value(forKey: "message") as! String)
                    
                    if dictData.value(forKey: "success") as! Bool == true{
                        
                        self.txtEmailFeedback.text = ""
                        self.txtViewFeedback.text = ""
                    }
                    
                    return
                }
            }
            
            ProjectUtility.displayTost(erroemessage: "We are are working on improvements. Please stay attached.")
        }) { (error) -> Void in
            
            print("error = ",error)
            ProjectUtility.loadingHide()
            ProjectUtility.displayTost(erroemessage: "We are are working on improvements. Please stay attached.")
        }
    }
    
    func loginFBJson(dictFBData: NSMutableDictionary)
    {
        ProjectUtility.loadingShow()
        
        let dict = ["facebook_id":dictFBData.value(forKey: "id") as! String, "name":dictFBData.value(forKey: "name") as! String, "os_type":"1", "version":"1.0", "profile_pic":dictFBData.value(forKey: "profilePic") as! String]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/login/loginfb"
        print("\(strUrl) == \(dict)")
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            ProjectUtility.loadingHide()
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    print("response = ",dictData)
                    
                    Constants.appDelegate.dictFBUserDetail = dictFBData
                    Constants.appDelegate.dictLoginUserDetail = dictData.value(forKey: "data") as! NSDictionary
                    
                    ProjectUtility.displayTost(erroemessage: dictData.value(forKey: "message") as! String)
                    
                    self.displayData()
                    
                    return
                }
            }
            
            ProjectUtility.displayTost(erroemessage: "We are are working on improvements. Please stay attached.")
            
            FBSDKAccessToken.setCurrent(nil)
            
        }) { (error) -> Void in
            
            print("error = ",error)
            
            ProjectUtility.loadingHide()
            FBSDKAccessToken.setCurrent(nil)
            ProjectUtility.displayTost(erroemessage: "We are are working on improvements. Please stay attached.")
        }
    }
    
    func getVideoListJson()
    {
        if self.arrLikeCommentCountList.count == 0{
            ProjectUtility.loadingShow()
        }
        
        var strVideoId = ""
        
        if btnVideos.isSelected == true {
            for i in 0 ..< self.arrVideoList.count {
                
                if strVideoId != "" {
                    strVideoId = strVideoId + ","
                }
                
                strVideoId = strVideoId + ((arrVideoList.object(at: i) as! NSDictionary).value(forKey: "id") as! String)
            }
        }else{
            for i in 0 ..< self.arrLiveVideoData.count {
                
                if strVideoId != "" {
                    strVideoId = strVideoId + ","
                }
                
                strVideoId = strVideoId + ((arrLiveVideoData.object(at: i) as! NSDictionary).value(forKey: "id") as! String)
            }
        }
        
        let dict = ["video_id":strVideoId, "facebook_id":Constants.appDelegate.dictFBUserDetail.value(forKey: "id") as! String]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/video/videoslist"
        print("\(strUrl) == \(dict)")
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            ProjectUtility.loadingHide()
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    print("response = ",dictData)
                    
                    if self.btnVideos.isSelected == true {
                        self.arrLikeCommentCountList = (dictData.value(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                    }else{
                        self.arrLikeCommentLiveVideoCountList = (dictData.value(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                    }
                    
                    self.tblList.reloadData()
                    
                    return
                }
            }
            
            if self.btnVideos.isSelected == true {
                
                if self.arrVideoList.count != 0{
                    ProjectUtility.displayTost(erroemessage: "We are are working on improvements. Please stay attached.")
                }
                
            }else{
                if self.arrLiveVideoData.count != 0{
                    ProjectUtility.displayTost(erroemessage: "We are are working on improvements. Please stay attached.")
                }
            }
            
        }) { (error) -> Void in
            
            print("error = ",error)
            
            ProjectUtility.loadingHide()
            ProjectUtility.displayTost(erroemessage: "We are are working on improvements. Please stay attached.")
        }
    }
    
    func likeDislikeJson(sender: UIButton)
    {
        ProjectUtility.loadingShow()
        
        var intLike = 0
        if sender.isSelected == false{
            intLike = 1
        }
        
        var strVideoId = ""
        if btnVideos.isSelected == true{
            strVideoId = (arrVideoList.object(at: sender.tag) as! NSDictionary).value(forKey: "id") as! String
        }else{
            strVideoId = (arrLiveVideoData.object(at: sender.tag) as! NSDictionary).value(forKey: "id") as! String
        }
        
        let dict = ["video_id":strVideoId, "like_status": intLike, "facebook_id":Constants.appDelegate.dictFBUserDetail.value(forKey: "id") as! String] as [String : Any]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/video/likevideo"
        print("\(strUrl) == \(dict)")
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            ProjectUtility.loadingHide()
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    print("response = ",dictData)
                    
                    var dictTemp = NSMutableDictionary()
                    
                    if self.btnVideos.isSelected == true{
                        dictTemp = (self.arrLikeCommentCountList.object(at: sender.tag) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    }else{
                        dictTemp = (self.arrLikeCommentLiveVideoCountList.object(at: sender.tag) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    }
                    
                    dictTemp.setValue(intLike, forKey: "like_status")
                    
                    var intLikes = 0
                    
                    if ((dictTemp.value(forKey: "likes") as? NSInteger) != nil) {
                        
                        intLikes = dictTemp.value(forKey: "likes") as! NSInteger
                    }else{
                        intLikes = Int(dictTemp.value(forKey: "likes") as! String)!
                    }
                    
                    if sender.isSelected == true{
                        
                        intLikes = intLikes - 1
                    }else{
                        intLikes = intLikes + 1
                    }
                    
                    dictTemp.setValue("\(intLikes)", forKey: "likes")
                    
                    if self.btnVideos.isSelected == true{
                        self.arrLikeCommentCountList.replaceObject(at: sender.tag, with: dictTemp)
                    }else{
                        self.arrLikeCommentLiveVideoCountList.replaceObject(at: sender.tag, with: dictTemp)
                    }
                    
                    self.tblList.reloadData()
                    
                    return
                }
            }
            
            ProjectUtility.displayTost(erroemessage: "We are are working on improvements. Please stay attached.")
            
        }) { (error) -> Void in
            
            print("error = ",error)
            
            ProjectUtility.loadingHide()
            ProjectUtility.displayTost(erroemessage: "We are are working on improvements. Please stay attached.")
        }
    }
    
    func incrementViewsJson(sender: UIButton)
    {
        var strVideoId = ""
        if btnVideos.isSelected == true{
            strVideoId = (arrVideoList.object(at: sender.tag) as! NSDictionary).value(forKey: "id") as! String
        }else{
            strVideoId = (arrLiveVideoData.object(at: sender.tag) as! NSDictionary).value(forKey: "id") as! String
        }
        
        let dict = ["video_id":strVideoId]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/video/incrementviews"
        print("\(strUrl) == \(dict)")
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            ProjectUtility.loadingHide()
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    print("response = ",dictData)
                    
                    var dictTemp = NSMutableDictionary()
                    
                    if self.btnVideos.isSelected == true{
                        dictTemp = (self.arrLikeCommentCountList.object(at: sender.tag) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    }else{
                        dictTemp = (self.arrLikeCommentLiveVideoCountList.object(at: sender.tag) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    }
                    
                    var intViews = 0
                    
                    if ((dictTemp.value(forKey: "views") as? NSInteger) != nil) {
                        
                        intViews = dictTemp.value(forKey: "views") as! NSInteger
                    }else{
                        intViews = Int(dictTemp.value(forKey: "views") as! String)!
                    }
                    
                    intViews = intViews + 1
                    
                    dictTemp.setValue("\(intViews)", forKey: "views")
                    
                    if self.btnVideos.isSelected == true{
                        self.arrLikeCommentCountList.replaceObject(at: sender.tag, with: dictTemp)
                    }else{
                        self.arrLikeCommentLiveVideoCountList.replaceObject(at: sender.tag, with: dictTemp)
                    }
                    
                    self.tblList.reloadData()
                    
                    return
                }
            }
        }) { (error) -> Void in
            
            print("error = ",error)
        }
    }
    
    
    //MARK:- Fetch events
    
    func fetchPageDetail(){
        
        ProjectUtility.loadingShow()
        FBSDKGraphRequest(graphPath: "/\(strPageId)", parameters: ["fields": "name, picture,cover"]).start(completionHandler: { (connection, result, error) -> Void in
            
            ProjectUtility.loadingHide()
            
            if (error == nil){
                print("PageDetail = ",result)
                
                self.dictPageData = (result as? NSDictionary)!
                ProjectUtility.loadingShow()
                self.fetchVideos()
            }else{
                
                print("error = ",error)
            }
        })
     }
    
    func fetchEvents(){
        
        ProjectUtility.loadingShow()
        FBSDKGraphRequest(graphPath: "/\(strPageId)/events", parameters: ["fields": "interested_count,start_time,description,name,place,attending_count"]).start(completionHandler: { (connection, result, error) -> Void in
            
            ProjectUtility.loadingHide()
            if (error == nil){
                
                print("Events = ",result)
                
                self.dictEventData = (result as? NSDictionary)!
                
                self.tblList.reloadData()
                
            }else{
                
                print("error = ",error)
            }
        })
    }
    
    func fetchVideos(){
//        ,embed_html,likes
        FBSDKGraphRequest(graphPath: "/\(strPageId)/videos", parameters: [
        "fields":"picture,description,updated_time,source,live_status"]).start(completionHandler: { (connection, result, error) -> Void in
            
            if (error == nil){
                
                ProjectUtility.loadingHide()
                print("Video = ",result)
                
                self.arrLiveVideoData = NSMutableArray()
                self.arrVideoList = NSMutableArray()
                self.arrLiveVideoData.add(((result as? NSDictionary)?.value(forKey: "data") as! NSArray).object(at: 0) as! NSDictionary)
                
                for i in 0 ..< ((result as? NSDictionary)?.value(forKey: "data") as! NSArray).count{
                    
                    if (((result as? NSDictionary)?.value(forKey: "data") as! NSArray).object(at: i) as! NSDictionary).value(forKey: "live_status") != nil && (((result as? NSDictionary)?.value(forKey: "data") as! NSArray).object(at: i) as! NSDictionary).value(forKey: "live_status") as! String == "LIVE" {
                        
                        self.arrLiveVideoData.add(((result as? NSDictionary)?.value(forKey: "data") as! NSArray).object(at: i) as! NSDictionary)
                    }
                    else if (((result as? NSDictionary)?.value(forKey: "data") as! NSArray).object(at: i) as! NSDictionary).value(forKey: "live_status") == nil || (((result as? NSDictionary)?.value(forKey: "data") as! NSArray).object(at: i) as! NSDictionary).value(forKey: "live_status") as! String != "LIVE_STOPPED"{
                        
                        self.arrVideoList.add(((result as? NSDictionary)?.value(forKey: "data") as! NSArray).object(at: i) as! NSDictionary)
                    }
                }
                
                if self.arrLiveVideoData.count != 0 && self.isBlinking == false{
                    self.blinkLiveButton()
                    self.lblLiveTitle.textColor = UIColor.red
                    
                    if self.btnLive.isSelected == true {
                        
                        if self.interstitial.isReady {
                            self.interstitial.present(fromRootViewController: self)
                        } else {
                            print("Ad wasn't ready")
                        }
                    }
                }
                
                if self.btnLive.isSelected == true{
                    if self.arrLiveVideoData.count == 0{
                        self.lblNoLiveSession.isHidden = false
                    }else{
                        self.lblNoLiveSession.isHidden = true
                    }
                }
                
                self.getVideoListJson()
                
                self.tblList.reloadData()
                
            }else{
                
                print("error = ",error)
            }
        })
    }
    
    //MARK:- Button TouchUp
    
    @IBAction func btnMenuAction(_ sender: UIButton){
        
        self.view.endEditing(true)
        
        if btnMenu.isSelected == false{
            
            self.viewMainMenu.frame = CGRect.init(x: 0, y: 0, width: Constants.ScreenSize.SCREEN_WIDTH, height: 0)
            
            scrlMainMenu.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                self.viewMainMenu.frame = CGRect.init(x: 0, y: 0, width: Constants.ScreenSize.SCREEN_WIDTH, height: Constants.ScreenSize.SCREEN_HEIGHT)
                self.view.layoutIfNeeded()
            })
            btnMenu.isSelected = true
        }
    }
    
    @IBAction func btnCloseMenuAction(_ sender: UIButton){
        
        self.view.endEditing(true)
        
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.viewMainMenu.frame = CGRect.init(x: 0, y: 0, width: Constants.ScreenSize.SCREEN_WIDTH, height: 0)
            self.view.layoutIfNeeded()
        })
        btnMenu.isSelected = false
    }
    
    
    @IBAction func btnVideosAction(_ sender: UIButton){
        
        self.view.endEditing(true)
        
        btnVideos.isSelected = true
        btnLive.isSelected = false
        btnEvents.isSelected = false
        
        imgVideos.image = UIImage(named: "video-selected-btn")
        imgLive.image = UIImage(named: "live-unselected")
        imgEvents.image = UIImage(named: "event-unselected-btn")
        
        if self.arrLiveVideoData.count == 0{
            lblLiveTitle.textColor = UIColor.init(red: 113.0/255.0, green: 118.0/255.0, blue: 133.0/255.0, alpha: 1.0)
        }
        
        if arrVideoList.count == 0{
            ProjectUtility.loadingShow()
            self.fetchVideos()
            return
        }
        self.tblList.reloadData()
    }
    
    @IBAction func btnLiveAction(_ sender: UIButton){
        
        self.view.endEditing(true)
        
        btnVideos.isSelected = false
        btnLive.isSelected = true
        btnEvents.isSelected = false
        
        if self.arrLiveVideoData.count == 0{
            
            lblLiveTitle.textColor = UIColor.init(red: 243.0/255.0, green: 192.0/255.0, blue: 96.0/255.0, alpha: 1.0)
        }
        
        imgVideos.image = UIImage(named: "video-unselected-btn")
        imgLive.image = UIImage(named: "live-selected")
        imgEvents.image = UIImage(named: "event-unselected-btn")
        
        if arrLiveVideoData.count == 0{
            ProjectUtility.loadingShow()
            self.fetchVideos()
            return
        }else if arrLikeCommentLiveVideoCountList.count == 0{
            self.getVideoListJson()
        }
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
        }
        
        self.tblList.reloadData()
    }
    
    @IBAction func btnEventsAction(_ sender: UIButton){
        
        self.view.endEditing(true)
        
        btnVideos.isSelected = false
        btnLive.isSelected = false
        btnEvents.isSelected = true
        
        if self.arrLiveVideoData.count == 0{
            lblLiveTitle.textColor = UIColor.init(red: 113.0/255.0, green: 118.0/255.0, blue: 133.0/255.0, alpha: 1.0)
        }
        
        imgVideos.image = UIImage(named: "video-unselected-btn")
        imgLive.image = UIImage(named: "live-unselected")
        imgEvents.image = UIImage(named: "event-selected-btn")
        
        if dictEventData.value(forKey: "data") == nil{
            self.fetchEvents()
            return
        }
        if dictEventData.value(forKey: "data") != nil{
            if (dictEventData.value(forKey: "data") as! NSArray).count == 0{
                self.fetchEvents()
                return
            }
        }
        self.tblList.reloadData()
    }
    
    //MARK: Main Menu
    @IBAction func btnMenuOptionAction(_ sender: UIButton){
        
        self.view.endEditing(true)
        
        if sender.tag == 1 {
            
            if(FBSDKAccessToken.current() == nil){
                ProjectUtility.displayTost(erroemessage: "You require login to use this feature")
                return
            }
            scrlMainMenu.setContentOffset(CGPoint(x: Constants.ScreenSize.SCREEN_WIDTH * 1, y: 0), animated: true)
            self.checkWeightTrackJson()
            
        }else if sender.tag == 2 {
            
            scrlMainMenu.setContentOffset(CGPoint(x: Constants.ScreenSize.SCREEN_WIDTH * 2, y: 0), animated: true)
        }else if sender.tag == 3 {
            
            if(FBSDKAccessToken.current() == nil){
                ProjectUtility.displayTost(erroemessage: "You require login to use this feature")
                return
            }
            
            scrlMainMenu.setContentOffset(CGPoint(x: Constants.ScreenSize.SCREEN_WIDTH * 3, y: 0), animated: true)
            self.listOfWightTrackJson()
            
        }else if sender.tag == 4 {
            
            scrlMainMenu.setContentOffset(CGPoint(x: Constants.ScreenSize.SCREEN_WIDTH * 4, y: 0), animated: true)
        }else if sender.tag == 5 {
            
            scrlMainMenu.setContentOffset(CGPoint(x: Constants.ScreenSize.SCREEN_WIDTH * 5, y: 0), animated: true)
            
            if(FBSDKAccessToken.current() != nil){
                
                txtEmailFeedback.text = Constants.appDelegate.dictFBUserDetail.value(forKey: "email") as? String
            }
            
        }else if sender.tag == 6 {
            if(FBSDKAccessToken.current() != nil){
                FBSDKAccessToken.setCurrent(nil)
                Constants.appDelegate.dictFBUserDetail = NSMutableDictionary()
                Constants.appDelegate.dictLoginUserDetail = NSMutableDictionary()
                UserDefaults.standard.removeObject(forKey: "UserDetail")
                _ = self.navigationController?.popViewController(animated: true)
            }else{
                self.loginWithFB()
            }
        }
    }
    
    @IBAction func btnMoveToMenuAction(_ sender: UIButton){
        
        self.view.endEditing(true)
        
        scrlMainMenu.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    //MARK: Weight Track
    
    @IBAction func btnSaveWeightTrackAction(_ sender: UIButton){
        
        self.view.endEditing(true)
        
        if txtWaistWeightTrack.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please enter waist")
            
        }else if txtHipsWeightTrack.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please enter hips")
            
        }else if txtArmWeightTrack.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please enter arm")
            
        }else if txtThighWeightTrack.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please enter thigh")
            
        }else if txtWeightWeightTrack.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please enter weight")
            
        }else{
            
            self.addWeightTrackJson()
        }
    }
    
    //MARK: BMI
    
    @IBAction func btnGenderAction(_ sender: UIButton){
        
        self.view.endEditing(true)
        
        if sender.tag == 1{
            
            if btnGender.isSelected == true {
                
                constGenderViewHeight.constant = 0
                btnGender.isSelected = false
            }else{
                
                constGenderViewHeight.constant = 90
                btnGender.isSelected = true
            }
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }else{
            if sender.tag == 2{
                txtGender.text = "Male"
            }else if sender.tag == 3{
                
                txtGender.text = "Female"
            }else if sender.tag == 4{
                
                txtGender.text = "Other"
            }
            constGenderViewHeight.constant = 0
            btnGender.isSelected = false
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func btnCalculateBMIAction(_ sender: UIButton){
        
        self.view.endEditing(true)
        
        if txtAge.text?.characters.count == 0{
            ProjectUtility.displayTost(erroemessage: "Please enter age")
        }else if txtGender.text?.characters.count == 0{
            ProjectUtility.displayTost(erroemessage: "Please select gender")
        }else if txtWeight.text?.characters.count == 0{
            ProjectUtility.displayTost(erroemessage: "Please enter weight")
        }else if txtHeight.text?.characters.count == 0{
            ProjectUtility.displayTost(erroemessage: "Please enter height")
        }else{
            let bmiObj = BMI()
            
            let height = (txtHeight.text?.floatValue)! / 100.0
            let weight = txtWeight.text?.floatValue
            
            bmiObj.units = "SI"
            bmiObj.age = (txtAge.text?.integerValue)!
            
            let bmiValue = bmiObj.getFromHeight(height, andWeight: weight!)
            
            if bmiValue != 0 && bmiValue > 10 && bmiValue < 100 {
                
                let bmiRange = bmiObj.getRangeFromBMI(bmiValue)
                
                btnCaluculateBMI.setTitle(NSString(format: "%.2f", bmiValue) as String, for: UIControlState.normal)
                lblBMIStatus.text = bmiRange
                lblBMIStatus.textColor = bmiObj.getColorFromRange(bmiRange)
            }else{
                
                btnCaluculateBMI.setTitle(NSString(format: "%.2f", bmiValue) as String, for: UIControlState.normal)
                lblBMIStatus.text = "Underweight"
                lblBMIStatus.textColor = bmiObj.getColorFromRange("Underweight")
            }
        }
    }
    
    //MARK: Feedback
    @IBAction func btnEditEmailFeedbackAction(_ sender: UIButton){
        
        txtEmailFeedback.becomeFirstResponder()
    }
    
    @IBAction func btnShootFeedbackAction(_ sender: UIButton){
        
        self.view.endEditing(true)
        
        if txtEmailFeedback.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please enter email address")
            
        }else if txtViewFeedback.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please enter your feedback")
        
        }else{
            
            self.sendFeedBackJson()
        }
    }
    
    //MARK: TableCell Event
    
    @IBAction func btnLikeAction(_ sender: UIButton){
        
        self.likeDislikeJson(sender: sender)
    }
    
    @IBAction func btnCommentAction(_ sender: UIButton){
        
        self.performSegue(withIdentifier: "comments", sender: sender)
    }
    
    @IBAction func btnPlayVideoAction(_ sender: UIButton){
        
        var videoURL = ""

        if btnVideos.isSelected == true{
            videoURL =  (arrVideoList.object(at: sender.tag) as! NSDictionary).value(forKey: "source") as! String
        }else if btnLive.isSelected == true{

//            192.168.0.113
            var strHostUrl = "http://192.168.2.60"
            
            if Constants.appDelegate.strLiveVideoURL != "" {
                strHostUrl = Constants.appDelegate.strLiveVideoURL
            }
            
            videoURL = "\(strHostUrl):1935/Fit4mii/myStream/playlist.m3u8?DVR"
        }
        
        let playerVC = MobilePlayerViewController(contentURL: NSURL.init(string: videoURL) as! URL)
        playerVC.title = "Fitness Player"
        playerVC.activityItems = [videoURL]
        playerVC.moviePlayer.controlStyle = MPMovieControlStyle.embedded
        playerVC.moviePlayer.isFullscreen = true
        presentMoviePlayerViewControllerAnimated(playerVC)
        
        Constants.appDelegate.isFullScreen = true
        
        self.incrementViewsJson(sender: sender)
    }
    
    //MARK:- TabeView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if tableView == tblList{
            if btnVideos.isSelected == true{
                
                return arrVideoList.count
                
            }else if btnLive.isSelected == true{
                
                return arrLiveVideoData.count
                
            }else if btnEvents.isSelected == true{
                
                if dictEventData.value(forKey: "data") != nil{
                    return (dictEventData.value(forKey: "data") as! NSArray).count
                }else{
                    return 0
                }
            }
        }else if tableView == tblWeeklyStatus{
            return arrWeeklyStatus.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
    
        if tableView == tblList{
         
            if btnVideos.isSelected == true{
                
                let cell:VideoListTableCell = tblList.dequeueReusableCell(withIdentifier: "VideoListTableCell", for: indexPath as IndexPath) as! VideoListTableCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                
                let dictTemp = arrVideoList.object(at: indexPath.row) as? NSDictionary
                
                if dictTemp?.value(forKey: "description") != nil{
                    cell.lblVideoDescription.text = dictTemp?.value(forKey: "description") as? String
                }else{
                    cell.lblVideoDescription.text = " "
                }
                
                cell.lblDate.text = ProjectUtility.changeDateFormate(strDate: dictTemp?.value(forKey: "updated_time") as! String , strFormatter1: "yyyy-MM-dd'T'HH:mm:ss+z", strFormatter2: "dd MMM yyyy HH:mm") as String
                
                cell.imgVideo.setImageWith(NSURL(string: dictTemp?.value(forKey: "picture") as! String) as URL!)
                
                cell.btnPlayVideo.tag = indexPath.row
                cell.btnPlayVideo.addTarget(self, action: #selector(EventVC.btnPlayVideoAction(_:)), for: UIControlEvents.touchUpInside)
                
//                if dictTemp?.value(forKey: "likes") != nil {
//                    
//                    cell.btnLikes.setTitle(NSString(format: "%d Likes", ((dictTemp?.value(forKey: "likes") as! NSDictionary).value(forKey: "data") as! NSArray).count) as String, for: UIControlState.normal)
//                }else{
//                    cell.btnLikes.setTitle("0", for: UIControlState.normal)
//                }
                
                cell.btnLikes.tag = indexPath.row
                cell.btnComments.tag = indexPath.row
                
                cell.btnLikes.addTarget(self, action: #selector(EventVC.btnLikeAction(_:)), for: UIControlEvents.touchUpInside)
                cell.btnComments.addTarget(self, action: #selector(EventVC.btnCommentAction(_:)), for: UIControlEvents.touchUpInside)
                
                if indexPath.row < arrLikeCommentCountList.count{
                
                    cell.btnViews.setTitle("\(((arrLikeCommentCountList.object(at: indexPath.row) as! NSDictionary).value(forKey: "views"))!) Views", for: UIControlState.normal)
                    
                    cell.btnLikes.setTitle("\(((arrLikeCommentCountList.object(at: indexPath.row) as! NSDictionary).value(forKey: "likes"))!) Likes", for: UIControlState.normal)
                    cell.btnLikes.setTitle("\(((arrLikeCommentCountList.object(at: indexPath.row) as! NSDictionary).value(forKey: "likes"))!) Likes", for: UIControlState.selected)
                    
                    cell.btnComments.setTitle("\(((arrLikeCommentCountList.object(at: indexPath.row) as! NSDictionary).value(forKey: "comments"))!) Comments", for: UIControlState.normal)
                    
                    if (((arrLikeCommentCountList.object(at: indexPath.row) as! NSDictionary).value(forKey: "like_status") as? NSInteger) != nil) {
                        
                        if (arrLikeCommentCountList.object(at: indexPath.row) as! NSDictionary).value(forKey: "like_status") as! NSInteger == 0 {
                            
                            cell.btnLikes.isSelected = false
                        }else{
                            cell.btnLikes.isSelected = true
                        }
                    }else{
                        if (arrLikeCommentCountList.object(at: indexPath.row) as! NSDictionary).value(forKey: "like_status") as! String == "0" {
                            
                            cell.btnLikes.isSelected = false
                        }else{
                            cell.btnLikes.isSelected = true
                        }
                    }
                }
                return cell
            }else if btnLive.isSelected == true{
                
                let cell:VideoListTableCell = tblList.dequeueReusableCell(withIdentifier: "VideoListTableCell", for: indexPath as IndexPath) as! VideoListTableCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                
                let dictTemp = arrLiveVideoData.object(at: indexPath.row) as? NSDictionary
                
                if dictTemp?.value(forKey: "description") != nil{
                    cell.lblVideoDescription.text = dictTemp?.value(forKey: "description") as? String
                }else{
                    cell.lblVideoDescription.text = " "
                }
                
                cell.lblDate.text = ProjectUtility.changeDateFormate(strDate: dictTemp?.value(forKey: "updated_time") as! String , strFormatter1: "yyyy-MM-dd'T'HH:mm:ss+z", strFormatter2: "dd MMM yyyy HH:mm") as String
                
                cell.imgVideo.setImageWith(NSURL(string: dictTemp?.value(forKey: "picture") as! String) as URL!)
                
                cell.btnPlayVideo.tag = indexPath.row
                cell.btnPlayVideo.addTarget(self, action: #selector(EventVC.btnPlayVideoAction(_:)), for: UIControlEvents.touchUpInside)
                
                if dictTemp?.value(forKey: "likes") != nil {
                    
                    cell.btnLikes.setTitle(NSString(format: "%d Likes", ((dictTemp?.value(forKey: "likes") as! NSDictionary).value(forKey: "data") as! NSArray).count) as String, for: UIControlState.normal)
                }else{
                    cell.btnLikes.setTitle("0", for: UIControlState.normal)
                }
                
                cell.btnLikes.tag = indexPath.row
                cell.btnComments.tag = indexPath.row
                
                cell.btnLikes.addTarget(self, action: #selector(EventVC.btnLikeAction(_:)), for: UIControlEvents.touchUpInside)
                cell.btnComments.addTarget(self, action: #selector(EventVC.btnCommentAction(_:)), for: UIControlEvents.touchUpInside)
                
                if arrLikeCommentLiveVideoCountList.count != 0 {
                    cell.btnViews.setTitle("\(((arrLikeCommentLiveVideoCountList.object(at: indexPath.row) as! NSDictionary).value(forKey: "views"))!) Views", for: UIControlState.normal)
                    
                    cell.btnLikes.setTitle("\(((arrLikeCommentLiveVideoCountList.object(at: indexPath.row) as! NSDictionary).value(forKey: "likes"))!) Likes", for: UIControlState.normal)
                    cell.btnLikes.setTitle("\(((arrLikeCommentLiveVideoCountList.object(at: indexPath.row) as! NSDictionary).value(forKey: "likes"))!) Likes", for: UIControlState.selected)
                    
                    cell.btnComments.setTitle("\(((arrLikeCommentLiveVideoCountList.object(at: indexPath.row) as! NSDictionary).value(forKey: "comments"))!) Comments", for: UIControlState.normal)
                    
                    if (((arrLikeCommentLiveVideoCountList.object(at: indexPath.row) as! NSDictionary).value(forKey: "like_status") as? NSInteger) != nil) {
                        
                        if (arrLikeCommentLiveVideoCountList.object(at: indexPath.row) as! NSDictionary).value(forKey: "like_status") as! NSInteger == 0 {
                            
                            cell.btnLikes.isSelected = false
                        }else{
                            cell.btnLikes.isSelected = true
                        }
                    }else{
                        if (arrLikeCommentLiveVideoCountList.object(at: indexPath.row) as! NSDictionary).value(forKey: "like_status") as! String == "0" {
                            
                            cell.btnLikes.isSelected = false
                        }else{
                            cell.btnLikes.isSelected = true
                        }
                    }
                }
                
                return cell
                
            }else if btnEvents.isSelected == true{
                
                let cell:EventListTableCell = tblList.dequeueReusableCell(withIdentifier: "EventListTableCell", for: indexPath as IndexPath) as! EventListTableCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                
                let dictTemp = (dictEventData.value(forKey: "data") as! NSArray).object(at: indexPath.row) as? NSDictionary
                
                cell.lblStartMonth.text = ProjectUtility.changeDateFormate(strDate: dictTemp?.value(forKey: "start_time") as! String , strFormatter1: "yyyy-MM-dd'T'HH:mm:ss+z", strFormatter2: "MMM") as String
                
                cell.lblStartDay.text = ProjectUtility.changeDateFormate(strDate: dictTemp?.value(forKey: "start_time") as! String , strFormatter1: "yyyy-MM-dd'T'HH:mm:ss+z", strFormatter2: "dd") as String
                
                cell.lblDescription.text = dictTemp?.value(forKey: "description") as? String
                
                cell.lblStartDate.text = ProjectUtility.changeDateFormate(strDate: dictTemp?.value(forKey: "start_time") as! String , strFormatter1: "yyyy-MM-dd'T'HH:mm:ss+z", strFormatter2: "dd MMM yyyy HH:mm") as String
                
                //            cell.lblAt.text = NSString(format: "At : %@", (dictTemp?.value(forKey: "place") as! NSDictionary).value(forKey: "name") as! String) as String
                
                let eventDate = ProjectUtility.dateFromString(strDate: dictTemp?.value(forKey: "start_time") as! String, strFormatter: "yyyy-MM-dd'T'HH:mm:ss+z")
                
                if eventDate > Date() {
                    cell.lblEventStatus.text = "Upcoming"
                }else{
                    cell.lblEventStatus.text = "Past"
                }
                
                if dictTemp?.value(forKey: "place") != nil && (dictTemp?.value(forKey: "place") as! NSDictionary).value(forKey: "location") != nil {
                    cell.lblLocation.text = NSString(format: "Location : %@ %@, %@", (dictTemp?.value(forKey: "place") as! NSDictionary).value(forKey: "name") as! String, ((dictTemp?.value(forKey: "place") as! NSDictionary).value(forKey: "location") as! NSDictionary).value(forKey: "city") as! String, ((dictTemp?.value(forKey: "place") as! NSDictionary).value(forKey: "location") as! NSDictionary).value(forKey: "country") as! String) as String
                }else{
                    cell.lblLocation.text = ""
                }
                
                
                cell.lblAttending.text = NSString(format: "Attending : %d", dictTemp?.value(forKey: "attending_count") as! NSInteger) as String
                
                cell.lblInterested.text = NSString(format: "Interested : %d", dictTemp?.value(forKey: "interested_count") as! NSInteger) as String
                
                return cell
                
            }
        }else if tableView == tblWeeklyStatus{
            let cell:WeeklyStatusTableCell = tblWeeklyStatus.dequeueReusableCell(withIdentifier: "WeeklyStatusTableCell", for: indexPath as IndexPath) as! WeeklyStatusTableCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            if Constants.ScreenSize.SCREEN_WIDTH != 375 {
                cell.constViewSpacing1.constant = 20
                cell.constViewSpacing2.constant = 20
                cell.constViewSpacing3.constant = 20
                cell.constViewSpacing4.constant = 20
            }
            
            cell.lblWeek.text = NSString(format: "Week %d", indexPath.row + 1) as String
            
            cell.lblWaist.text = NSString(format: "%.2f", (arrWeeklyStatus.object(at: indexPath.row) as! NSDictionary).value(forKey: "waist") as! CGFloat) as String
            
            cell.lblHips.text = NSString(format: "%.2f", (arrWeeklyStatus.object(at: indexPath.row) as! NSDictionary).value(forKey: "hips") as! CGFloat) as String
            
            cell.lblArm.text = NSString(format: "%.2f", (arrWeeklyStatus.object(at: indexPath.row) as! NSDictionary).value(forKey: "arm") as! CGFloat) as String
            
            cell.lblThigh.text = NSString(format: "%.2f", (arrWeeklyStatus.object(at: indexPath.row) as! NSDictionary).value(forKey: "thigh") as! CGFloat) as String
            
            cell.lblWeight.text = NSString(format: "%.2f", (arrWeeklyStatus.object(at: indexPath.row) as! NSDictionary).value(forKey: "weight") as! CGFloat) as String
            
            return cell
        }
        
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath as IndexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 0.0
    }
    
    //MARK:- FB Login
    
    func loginWithFB(){
        
        let login = FBSDKLoginManager()
        login.logIn(withReadPermissions: ["public_profile"], from: self) { (result, error) in
            
            if error != nil {
                print("Process error")
            }
            else if (result?.isCancelled)! {
                print("Cancelled")
            }
            else {
                print("Logged in")
                
                if(FBSDKAccessToken.current() != nil){
                    
                    ProjectUtility.loadingShow()
                    
                    FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email, picture.type(normal)"]).start(completionHandler: { (connection, result, error) -> Void in
                        
                        ProjectUtility.loadingHide()
                        
                        if (error == nil){
                            print("ProfileInfo = ",result)
                            
                            let dictFbData = NSMutableDictionary()
                            dictFbData.setValue((result as! NSDictionary).value(forKey: "id") as! String, forKey: "id")
                            dictFbData.setValue(NSString (format: "%@ %@",(result as! NSDictionary).value(forKey: "first_name") as! String, (result as! NSDictionary).value(forKey: "last_name") as! String), forKey: "name")
                            dictFbData.setValue((result as! NSDictionary).value(forKey: "email") as! String, forKey: "email")
                            dictFbData.setValue((((result as! NSDictionary).value(forKey: "picture") as! NSDictionary).value(forKey: "data") as! NSDictionary).value(forKey: "url") as! String, forKey: "profilePic")
                            
                            self.loginFBJson(dictFBData: dictFbData)
                        }
                    })
                }
            }
        }
    }
    
    //MARL:- Blick Live Button
    
    func blinkLiveButton(){
        self.isBlinking = true
        lblLiveTitle.alpha = 0.0
        UILabel.animate(withDuration: 1, animations: {
            self.lblLiveTitle.alpha = 1.0
            }, completion: {
                (value: Bool) in
                if self.arrLiveVideoData.count != 0{
                    self.blinkLiveButton()
                }else{
                    self.lblLiveTitle.textColor = UIColor.init(red: 113.0/255.0, green: 118.0/255.0, blue: 133.0/255.0, alpha: 1.0)
                    self.isBlinking = false
                }
        })
        
    }
    
    // MARK:- Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        tblList.tag = ((sender as? UIButton)?.tag)!
        
        let vc: CommentVC = segue.destination as! CommentVC
        
        if btnVideos.isSelected == true {
            vc.dictSelectedVideo = (arrLikeCommentCountList.object(at: ((sender as? UIButton)?.tag)!) as! NSDictionary).mutableCopy() as! NSMutableDictionary
        }else{
            vc.dictSelectedVideo = (arrLikeCommentLiveVideoCountList.object(at: ((sender as? UIButton)?.tag)!) as! NSDictionary).mutableCopy() as! NSMutableDictionary
        }
    }
    
    // MARK:- Notification
    
    func commentNotification(notification: NSNotification){
        
        let dict = notification.object as! NSMutableDictionary
        
        if btnVideos.isSelected == true {
            arrLikeCommentCountList.replaceObject(at: tblList.tag, with: dict)
        }else{
            arrLikeCommentLiveVideoCountList.replaceObject(at: tblList.tag, with: dict)
        }
        
        tblList.reloadData()
    }
    
    // MARK:- BannerADDelegate
    
    /// Tells the delegate an ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("interstitialDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        print("interstitialWillPresentScreen")
    }
    
    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        print("interstitialWillDismissScreen")
    }
    
    /// Tells the delegate the interstitial had been animated off the screen.
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        print("interstitialDidDismissScreen")
        interstitial = createAndLoadInterstitial()
    }
    
    /// Tells the delegate that a user click will open another app
    /// (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        print("interstitialWillLeaveApplication")
    }
}

class VideoListTableCell: UITableViewCell {
        
    @IBOutlet var lblVideoDescription: UILabel!
    @IBOutlet var lblDate: UILabel!
    
    @IBOutlet var imgVideo: UIImageView!
    @IBOutlet var btnPlayVideo: UIButton!
    
    @IBOutlet var btnViews: UIButton!
    @IBOutlet var btnLikes: UIButton!
    @IBOutlet var btnComments: UIButton!
    
    override func awakeFromNib(){
        super.awakeFromNib()
    }
}

class EventListTableCell: UITableViewCell {
    
    @IBOutlet var lblStartMonth: UILabel!
    @IBOutlet var lblStartDay: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var lblStartDate: UILabel!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var lblAttending: UILabel!
    @IBOutlet var lblInterested: UILabel!
    @IBOutlet var lblEventStatus: UILabel!
}

class TableCell: UITableViewCell {
}

class WeeklyStatusTableCell: UITableViewCell {
    
    @IBOutlet var lblWeek: UILabel!
    @IBOutlet var lblWaist: UILabel!
    @IBOutlet var lblHips: UILabel!
    @IBOutlet var lblArm: UILabel!
    @IBOutlet var lblThigh: UILabel!
    @IBOutlet var lblWeight: UILabel!
    
    @IBOutlet var constViewSpacing1: NSLayoutConstraint!
    @IBOutlet var constViewSpacing2: NSLayoutConstraint!
    @IBOutlet var constViewSpacing3: NSLayoutConstraint!
    @IBOutlet var constViewSpacing4: NSLayoutConstraint!
    
}
