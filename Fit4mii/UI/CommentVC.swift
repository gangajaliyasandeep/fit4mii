//
//  CommentVC.swift
//  Fit4mii
//
//  Created by Sandeep Gangajaliya on 23/05/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit

class CommentVC: UIViewController {
    
    //MARK:- Outlet Declaration
    @IBOutlet var tblComments: UITableView!
    
    @IBOutlet var txtViewComment: UITextView!
    
    //MARK: Other Variable
    var arrCommentList = NSMutableArray()
    var dictSelectedVideo = NSMutableDictionary()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblComments.rowHeight = UITableViewAutomaticDimension
        tblComments.estimatedRowHeight = 52.0
        
        txtViewComment.layer.cornerRadius = 5.0
        txtViewComment.layer.borderColor = UIColor.lightGray.cgColor
        txtViewComment.layer.borderWidth = 1.0
        
        self.commentListJson()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Web Service Call
    func commentListJson()
    {
        ProjectUtility.loadingShow()
        
        let dict = ["video_id":"\(dictSelectedVideo.value(forKey: "video_id")!)"]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/video/videoscommentlist"
        print("\(strUrl) == \(dict)")
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            ProjectUtility.loadingHide()
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    print("response = ",dictData)
                    
                    if dictData.value(forKey: "success") as! Bool == true{
                        
                        self.arrCommentList = (dictData.value(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                        
                        self.tblComments.reloadData()
                        
                        let lastRowIndex = self.tblComments.numberOfRows(inSection: 0) - 1
                        let pathToLastRow = IndexPath.init(row: lastRowIndex, section: 0)
                        
                        self.tblComments.scrollToRow(at: pathToLastRow, at: UITableViewScrollPosition.none, animated: true)
                    }
                    
                    return
                }
            }
            
            ProjectUtility.displayTost(erroemessage: "We are are working on improvements. Please stay attached.")
        }) { (error) -> Void in
            
            print("error = ",error)
            ProjectUtility.loadingHide()
            ProjectUtility.displayTost(erroemessage: "We are are working on improvements. Please stay attached.")
        }
    }
    
    func addCommentJson()
    {
        ProjectUtility.loadingShow()
        
        let dict = ["video_id":"\(dictSelectedVideo.value(forKey: "video_id")!)", "facebook_id":Constants.appDelegate.dictFBUserDetail.value(forKey: "id") as! String, "comment":txtViewComment.text!]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/video/commentvideo"
        print("\(strUrl) == \(dict)")
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            ProjectUtility.loadingHide()
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    print("response = ",dictData)
                    
                    if dictData.value(forKey: "success") as! Bool == true{
                        
                        let dictCommentData = NSMutableDictionary()
                        dictCommentData.setValue("test", forKey: "name")
                        dictCommentData.setValue(self.txtViewComment.text, forKey: "text")
                        
                        self.arrCommentList.add(dictCommentData)
                        
                        self.tblComments.reloadData()
                        
                        self.txtViewComment.text = ""
                        
                        let lastRowIndex = self.tblComments.numberOfRows(inSection: 0) - 1
                        let pathToLastRow = IndexPath.init(row: lastRowIndex, section: 0)
                        
                        self.tblComments.scrollToRow(at: pathToLastRow, at: UITableViewScrollPosition.none, animated: true)
                        
                        var intViews = 0
                        
                        if ((self.dictSelectedVideo.value(forKey: "comments") as? NSInteger) != nil) {
                            
                            intViews = self.dictSelectedVideo.value(forKey: "comments") as! NSInteger
                        }else{
                            intViews = Int(self.dictSelectedVideo.value(forKey: "comments") as! String)!
                        }
                        
                        intViews = intViews + 1
                        
                        self.dictSelectedVideo.setValue("\(intViews)", forKey: "comments")
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "commentNotification"), object: self.dictSelectedVideo)
                    }
                    
                    return
                }
            }
            
            ProjectUtility.displayTost(erroemessage: "We are are working on improvements. Please stay attached.")
        }) { (error) -> Void in
            
            print("error = ",error)
            ProjectUtility.loadingHide()
            ProjectUtility.displayTost(erroemessage: "We are are working on improvements. Please stay attached.")
        }
    }
    
    //MARK:- Button TouchUp
    
    @IBAction func btnBackAction(_ sender: UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPostAction(_ sender: UIButton){
        
        self.view.endEditing(true)
        if txtViewComment.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please enter your comment")
        }else{
            self.addCommentJson()
        }
    }
    
    //MARK:- TabeView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
                
        return arrCommentList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell{
                            
        let cell:CommentsTableCell = tableView.dequeueReusableCell(withIdentifier: "CommentsTableCell", for: indexPath as IndexPath) as! CommentsTableCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        cell.lblName.text = (arrCommentList.object(at: indexPath.row) as! NSDictionary).value(forKey: "name") as? String
        
        cell.lblDescription.text = (arrCommentList.object(at: indexPath.row) as! NSDictionary).value(forKey: "text") as? String
        
        
        cell.imgBG.layer.shadowRadius = 4
        cell.imgBG.layer.shadowOffset = CGSize.init(width: 0, height: 8)
        cell.imgBG.layer.shadowColor = UIColor.init(red: 39.0/255.0, green: 46.0/255.0, blue: 54.0/255.0, alpha: 1.0).cgColor
        cell.imgBG.layer.shadowOpacity = 0.75
        
        return cell
    }
}

class CommentsTableCell: UITableViewCell {
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblDescription: UILabel!
    
    @IBOutlet var imgBG: UIImageView!
}
