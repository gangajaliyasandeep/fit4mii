//
//  ViewController.swift
//  Fit4mii
//
//  Created by Sandeep Gangajaliya on 28/02/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import MediaPlayer

class ViewController: UIViewController {

    //MARK:- Outlet
    @IBOutlet var imgLogo: UIImageView!
    
    @IBOutlet var btnLoginWithFacebook: UIButton!
    @IBOutlet var btnGuest: UIButton!
    
    var avPlayer = AVPlayer()
    
    var isFirstTime = true
    
    @IBOutlet var playerView: YTPlayerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnLoginWithFacebook.layer.cornerRadius = 21.0
        btnLoginWithFacebook.clipsToBounds = true
        
        btnGuest.layer.cornerRadius = 21.0
        btnGuest.clipsToBounds = true
        
        let filepath: String? = Bundle.main.path(forResource: "Fit4Mii_Video", ofType: "mov")
        let fileURL = URL(fileURLWithPath: filepath!)
        avPlayer = AVPlayer.init(url: fileURL)
        avPlayer.actionAtItemEnd = AVPlayerActionAtItemEnd.none
        let videoLayer = AVPlayerLayer.init(player: avPlayer)
        videoLayer.frame = view.frame
        videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        view.layer.addSublayer(videoLayer)
        avPlayer.play()
        avPlayer.actionAtItemEnd = AVPlayerActionAtItemEnd.none
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.playerItemDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: avPlayer.currentItem)

        self.view.bringSubview(toFront: imgLogo)
        self.view.bringSubview(toFront: btnLoginWithFacebook)
        self.view.bringSubview(toFront: btnGuest)
        
        /*self.playerView.load(withVideoId: "CvA333bjuU8")
        
        let loginButton = FBSDKLoginButton()
        loginButton.center = self.view.center
        loginButton.readPermissions = ["public_profile", "email", "user_friends"]
        self.view.addSubview(loginButton)
        
        FBSDKProfile.enableUpdates(onAccessTokenChange: true)*/
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        avPlayer.play()
        
        if isFirstTime == true && Constants.appDelegate.dictLoginUserDetail.value(forKey: "user_id") != nil{
                
            self.fetchFacebookDetail(intType: 0)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        avPlayer.pause()
    }
    
    // MARK:- Webservice Call
    
    func loginFBJson(dictFBData: NSMutableDictionary)
    {
        ProjectUtility.loadingShow()
        
        let dict = ["facebook_id":dictFBData.value(forKey: "id") as! String, "name":dictFBData.value(forKey: "name") as! String, "os_type":"1", "version":"1.0", "profile_pic":dictFBData.value(forKey: "profilePic") as! String]
        
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/login/loginfb"
        print("\(strUrl) == \(dict)")
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        webserviceCall.post(NSURL(string: strUrl) as URL!, parameters: dict as [NSObject: AnyObject]!, withSuccessHandler: { (response) -> Void in
            
            ProjectUtility.loadingHide()
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    print("response = ",dictData)
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = (storyboard.instantiateViewController(withIdentifier: "EventVC") as! EventVC)
                    self.navigationController?.pushViewController(controller, animated: true)
                    
                    Constants.appDelegate.dictFBUserDetail = dictFBData
                    Constants.appDelegate.dictLoginUserDetail = dictData.value(forKey: "data") as! NSDictionary
                    
                    let archivedUser = NSKeyedArchiver.archivedData(withRootObject: Constants.appDelegate.dictLoginUserDetail)
                    UserDefaults.standard.setValue(archivedUser, forKey: "UserDetail")
                    UserDefaults.standard.synchronize()
                    
                    
                    ProjectUtility.displayTost(erroemessage: dictData.value(forKey: "message") as! String)
                    
                    self.isFirstTime = false
                    
                    return
                }
            }
            
            ProjectUtility.displayTost(erroemessage: "We are are working on improvements. Please stay attached.")
            
            FBSDKAccessToken.setCurrent(nil)
            
        }) { (error) -> Void in
            
            print("error = ",error!)
            
            ProjectUtility.loadingHide()
            
            FBSDKAccessToken.setCurrent(nil)
            
            ProjectUtility.displayTost(erroemessage: "We are are working on improvements. Please stay attached.")
        }
    }
    
    //MARK:- Button TouchUp
    
    @IBAction func btnFacebookAction (_ sender: UIButton){
        
        let login = FBSDKLoginManager()
        login.logIn(withReadPermissions: ["public_profile"], from: self) { (result, error) in
            
            if error != nil {
                print("Process error")
            }
            else if (result?.isCancelled)! {
                print("Cancelled")
            }
            else {
                print("Logged in")
                
                self.fetchFacebookDetail(intType: 1)
            }
        }
    }
    
    @IBAction func btnGuestAction (_ sender: UIButton){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = (storyboard.instantiateViewController(withIdentifier: "EventVC") as! EventVC)
        self.navigationController?.pushViewController(controller, animated: true)
//        http://:1935/Fit4mii/myStream/playlist.m3u8?DVR
//        let videoURL =  "http://192.168.0.113:1935/Fit4mii/myStream/playlist.m3u8?DVR"
//        let playerVC = MobilePlayerViewController(contentURL: NSURL.init(string: videoURL) as! URL)
//        playerVC.title = "Fitness Player"
//        playerVC.activityItems = [videoURL]
//        playerVC.moviePlayer.controlStyle = MPMovieControlStyle.embedded
//        playerVC.moviePlayer.isFullscreen = true
//        presentMoviePlayerViewControllerAnimated(playerVC)
//        
//        Constants.appDelegate.isFullScreen = true
    }
    
    // MARK: - Fetch Facebook Detail
    
    func fetchFacebookDetail(intType: NSInteger) {
        
        if(FBSDKAccessToken.current() != nil){
        
            ProjectUtility.loadingShow()
            
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email, picture.type(normal)"]).start(completionHandler: { (connection, result, error) -> Void in
                
                ProjectUtility.loadingHide()
                
                if (error == nil){
                    print("ProfileInfo = ",result)
                    
                    let dictFbData = NSMutableDictionary()
                    dictFbData.setValue((result as! NSDictionary).value(forKey: "id") as! String, forKey: "id")
                    dictFbData.setValue(NSString (format: "%@ %@",(result as! NSDictionary).value(forKey: "first_name") as! String, (result as! NSDictionary).value(forKey: "last_name") as! String), forKey: "name")
                    
                    if (result as! NSDictionary).value(forKey: "email") != nil{
                        dictFbData.setValue((result as! NSDictionary).value(forKey: "email") as! String, forKey: "email")
                    }else{
                        dictFbData.setValue("", forKey: "email")
                    }
                    dictFbData.setValue((((result as! NSDictionary).value(forKey: "picture") as! NSDictionary).value(forKey: "data") as! NSDictionary).value(forKey: "url") as! String, forKey: "profilePic")
                    
                    if intType == 1{
                        self.loginFBJson(dictFBData: dictFbData)
                        
                    }else{
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let controller = (storyboard.instantiateViewController(withIdentifier: "EventVC") as! EventVC)
                        self.navigationController?.pushViewController(controller, animated: true)
                        Constants.appDelegate.dictFBUserDetail = dictFbData
                        
                        self.isFirstTime = false

                    }
                }
            })
        }
    }

    // MARK: - Repeat Video

    func playerItemDidReachEnd(_ notification: Notification) {
        let p: AVPlayerItem? = notification.object as! AVPlayerItem?
        p?.seek(to: kCMTimeZero)
    }
}

