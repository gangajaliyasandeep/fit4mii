//
//  AppDelegate.swift
//  Fit4mii
//
//  Created by Sandeep Gangajaliya on 28/02/17.
//  Copyright © 2017 Sandeep Gangajaliya. All rights reserved.
//

import UIKit
import CoreData
import MediaPlayer
import GoogleMobileAds

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var dictFBUserDetail = NSMutableDictionary()
    var dictLoginUserDetail = NSDictionary()
    
    var strLiveVideoURL = ""
    
    var isFullScreen = false

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        IQKeyboardManager.sharedManager().enable = true
        
        if UserDefaults.standard.value(forKey: "UserDetail") != nil {
            
            let unarchivedObject = UserDefaults.standard.object(forKey: "UserDetail") as? NSData
            
            dictLoginUserDetail = (NSKeyedUnarchiver.unarchiveObject(with: unarchivedObject! as Data) as? NSDictionary)!
            
            print(dictLoginUserDetail)
        }
        
        if UserDefaults.standard.value(forKey: "LiveVideoURL") != nil {
            
            let unarchivedObject = UserDefaults.standard.object(forKey: "LiveVideoURL") as? NSData
            
            strLiveVideoURL = (NSKeyedUnarchiver.unarchiveObject(with: unarchivedObject! as Data) as? String)!
            
            print(dictLoginUserDetail)
        }
        
        self.getLiveVideoURLJson()
        GADMobileAds.configure(withApplicationID: "ca-app-pub-4067556366564738/9244465206")
        
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        let handled: Bool = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        // Add any custom logic here.
        
        return handled
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) ->UIInterfaceOrientationMask {
        
        if isFullScreen == true{
            return [UIInterfaceOrientationMask.portrait, UIInterfaceOrientationMask.landscapeLeft, UIInterfaceOrientationMask.landscapeRight]
        }else{
            return [UIInterfaceOrientationMask.portrait]
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Fit4mii")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    // MARK:- Webservice Call
    func getLiveVideoURLJson()
    {
        let strUrl = "\(ProjectSharedObj.sharedInstance.baseUrl)/video/urlsetting"
        print("\(strUrl)")
        
        let webserviceCall = WebserviceCall()
        webserviceCall.headerFieldsDict = ["Content-Type": "application/json"]
        
        webserviceCall.get(NSURL(string: strUrl) as URL!, parameters: nil, withSuccessHandler: { (response) -> Void in
            
            if response?.webserviceResponse != nil{
                
                if let dictData = response?.webserviceResponse as? NSDictionary{
                    
                    print("response = ",dictData)
                    
                    if dictData.value(forKey: "success") as! Bool == true{
                        
                        self.strLiveVideoURL = dictData.value(forKey: "url") as! String
                        
                        let archivedUser = NSKeyedArchiver.archivedData(withRootObject: self.strLiveVideoURL)
                        UserDefaults.standard.setValue(archivedUser, forKey: "LiveVideoURL")
                        UserDefaults.standard.synchronize()
                    }
                    
                    return
                }
            }
        }) { (error) -> Void in
            
            print("error = ",error)
            ProjectUtility.loadingHide()
        }
    }
}

class ProjectUtility: NSObject
{
    class func loadingShow()
    {
        let loader = Loader()
        loader.show(UIColor.black)
    }
    
    class func loadingHide()
    {
        let loader = Loader()
        loader.hide()
    }
    
    class func changeDateFormate (strDate: String, strFormatter1 strDateFormatter1: String, strFormatter2 strDateFormatter2: String) -> NSString
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = strDateFormatter1
        
        if let date = dateFormatter.date(from: strDate)
        {
            dateFormatter.dateFormat = strDateFormatter2
            
            if let strConvertedDate:NSString = dateFormatter.string(from: date) as NSString?
            {
                return strConvertedDate
            }
        }
        return ""
    }
    
    class func dateFromString (strDate: String, strFormatter strDateFormatter: String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = strDateFormatter
        
        if let convertedDate = dateFormatter.date(from: strDate)
        {
            return convertedDate
        }
        return Date()
    }
    
    class func displayTost(erroemessage: String)
    {
        let appDelegate = (UIApplication.shared.delegate as? AppDelegate)!
        
        let style = CSToastStyle.init(defaultStyle: ())
        style?.messageFont = UIFont.boldSystemFont(ofSize: 15.0)
        style?.messageColor = UIColor.white
        style?.messageAlignment = .center
        style?.titleAlignment = .center
        style?.backgroundColor = UIColor.init(red: 243.0 / 255.0, green: 192.0 / 255.0, blue: 96.0 / 255.0, alpha: 1.0)
        appDelegate.window?.makeToast(erroemessage, duration: 2.5, position: CSToastPositionBottom, style: style)
    }
    
    class func imageShadow(image: UIImageView)
    {
        image.layer.shadowRadius = 6
        image.layer.shadowOffset = CGSize.init(width: 0, height: 8)
        image.layer.shadowColor = UIColor.init(red: 39.0/255.0, green: 46.0/255.0, blue: 54.0/255.0, alpha: 1.0).cgColor
        image.layer.shadowOpacity = 0.75
    }
    
    
}

class ProjectSharedObj: NSObject
{
    static let sharedInstance = ProjectSharedObj()
    
    var baseUrl = "http://77.68.80.170"
}

class Constants: NSObject
{
    static let appDelegate = (UIApplication.shared.delegate as? AppDelegate)!
    
    struct ScreenSize {
        static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
}

extension String {
    var floatValue: Float {
        return (self as NSString).floatValue
    }
}

extension String {
    var integerValue: NSInteger {
        return (self as NSString).integerValue
    }
}
