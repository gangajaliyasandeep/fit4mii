//
//  BMI.m
//  BMICalc
//
//  Created by Soroush Pour on 15/02/13.
//  Copyright (c) 2013 Soroush Pour. All rights reserved.
//

#import "BMI.h"

@implementation BMI

- (id)init {
    self = [super init];
    
    if(self) {
        [self setUnits: @"SI"];
    }
    
    return self;
}

- (float) getBMIFromHeight:(float)height andWeight:(float) weight {
    
    if([[self units] isEqual: @"SI"]) {
        return weight/(height * height);
    }
    
    if([[self units] isEqual: @"EE"]) {
        return weight/(height * height);// * 703.06957964;
    }
    
    return 0;

}

- (NSString*) getRangeFromBMI:(float)BMI {

    if (self.age <= 18) {
        if (BMI < 15) return @"Underweight";
        if(BMI>=15 && BMI<19) return @"Normal";
        if(BMI>=19 && BMI<25) return @"Overweight";
        if(BMI>=25) return @"Obese";
    }else if (self.age >= 19 && self.age <= 24){
        if(BMI<19) return @"Underweight";
        if(BMI>=19 && BMI<25) return @"Normal";
        if(BMI>=25 && BMI<30) return @"Overweight";
        if(BMI>=30) return @"Obese";
    }else if (self.age >= 25 && self.age <= 34){
        if(BMI<20) return @"Underweight";
        if(BMI>=20 && BMI<26) return @"Normal";
        if(BMI>=26 && BMI<30) return @"Overweight";
        if(BMI>=30) return @"Obese";
    }else if (self.age >= 35 && self.age <= 44){
        if(BMI<21) return @"Underweight";
        if(BMI>=21 && BMI<27) return @"Normal";
        if(BMI>=27 && BMI<30) return @"Overweight";
        if(BMI>=30) return @"Obese";
    }else if (self.age >= 45 && self.age <= 54){
        if(BMI<22) return @"Underweight";
        if(BMI>=22 && BMI<27) return @"Normal";
        if(BMI>=27 && BMI<=30) return @"Overweight";
        if(BMI>30) return @"Obese";
    }else if (self.age >= 55 && self.age <= 64){
        if(BMI<23) return @"Underweight";
        if(BMI>=23 && BMI<29) return @"Normal";
        if(BMI>=29 && BMI<=30) return @"Overweight";
        if(BMI>30) return @"Obese";
    }else if (self.age > 64){
        if(BMI<24) return @"Underweight";
        if(BMI>=24 && BMI<30) return @"Normal";
        if(BMI>=30 && BMI<=32) return @"Overweight";
        if(BMI>32) return @"Obese";
    }
    
    return @"Undefined";
}

- (UIColor*) getColorFromRange:(NSString*)range {
    
    if([range isEqual: @"Underweight"] || [range isEqual: @"Overweight"]) {
        return [UIColor orangeColor];
    }
    
    if([range isEqual: @"Obese"]) {
        return [UIColor redColor];
    }
    
    if([range isEqual: @"Normal"]) {
        return [UIColor greenColor];
    }
    
    return [UIColor greenColor];
    
}

@end
